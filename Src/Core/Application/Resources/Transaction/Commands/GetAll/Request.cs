﻿namespace Monosoft.PayLike.Application.Resources.Transaction.Commands.GetAll
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public string MerchantId { get; set; }

        public void Validate()
        {
            if(string.IsNullOrEmpty(MerchantId))
            {
                throw new ValidationException(nameof(MerchantId));
            }
        }
    }
}