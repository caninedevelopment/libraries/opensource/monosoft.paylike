namespace Monosoft.PayLike.Application.Resources.Transaction.Commands.GetAll
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.PayLike.Domain.Interfaces;

    public class Command : IFunction<Request, Response>
    {
        private readonly IPayLikeTransaction trans;

        public Command(
            IPayLikeTransaction trans)
        {
            this.trans = trans;
        }

        public Response Execute(Request input)
        {
            var transactions = trans.GetAll(input.MerchantId);

            return new Response(transactions);
        }
    }
}