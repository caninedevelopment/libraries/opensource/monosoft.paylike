namespace Monosoft.PayLike.Application.Resources.Transaction.Commands.GetAll
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.PayLike.Application.Resources.Transaction.Common;
    using Monosoft.PayLike.Domain.Entities.Transaction;
    using System.Collections.Generic;
    using System.Linq;

    public class Response : IDtoResponse
    {
        public List<TransactionDTO> Transactions { get; set; }

        public Response(List<Transaction> transactions)
        {
            Transactions = transactions.Select(x => new TransactionDTO(x)).ToList();
        }
    }
}