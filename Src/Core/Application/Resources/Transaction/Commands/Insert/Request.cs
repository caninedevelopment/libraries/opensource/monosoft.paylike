namespace Monosoft.PayLike.Application.Resources.Transaction.Commands.Insert
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public string MerchantId { get; set; }
        public string CardId { get; set; }
        public string Descriptor { get; set; }
        public string Currency { get; set; }
        public int AmountAsMinor { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(MerchantId))
            {
                throw new ValidationException(nameof(MerchantId));
            }

            if (string.IsNullOrEmpty(CardId))
            {
                throw new ValidationException(nameof(CardId));
            }

            if (string.IsNullOrEmpty(Currency))
            {
                throw new ValidationException(nameof(Currency));
            }

            if (AmountAsMinor < 1)
            {
                throw new ValidationException(nameof(AmountAsMinor));
            }
        }
    }
}