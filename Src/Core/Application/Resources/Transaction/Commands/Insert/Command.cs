namespace Monosoft.PayLike.Application.Resources.Transaction.Commands.Insert
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.PayLike.Domain.Entities.Transaction;
    using Monosoft.PayLike.Domain.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly IPayLikeTransaction trans;

        public Command(
            IPayLikeTransaction trans)
        {
            this.trans = trans;
        }

        public void Execute(Request input)
        {
            Transaction transaction = new Transaction(input.CardId, input.Currency, input.AmountAsMinor, input.Descriptor);

            trans.Insert(input.MerchantId, transaction);
        }
    }
}