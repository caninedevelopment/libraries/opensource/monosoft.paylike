namespace Monosoft.PayLike.Application.Resources.Transaction.Commands.Update
{
    using Monosoft.Common.Command.Interfaces;

    public class Command : IProcedure<Request>
    {
        public Command()
        {
        }

        public void Execute(Request input)
        {
        }
    }
}