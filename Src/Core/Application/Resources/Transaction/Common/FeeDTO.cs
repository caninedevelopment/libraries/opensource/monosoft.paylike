﻿using Monosoft.PayLike.Domain.Entities.Transaction;

namespace Monosoft.PayLike.Application.Resources.Transaction.Common
{
    public class FeeDTO
    {
        public int Flat { get; set; }
        public int Rate { get; set; }

        public FeeDTO(Fee fee)
        {
            this.Flat = fee.Flat;
            this.Rate = fee.Rate;
        }
    }
}
