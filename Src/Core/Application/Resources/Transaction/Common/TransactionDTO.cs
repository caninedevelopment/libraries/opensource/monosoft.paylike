﻿namespace Monosoft.PayLike.Application.Resources.Transaction.Common
{
    using Monosoft.PayLike.Domain.Entities.Transaction;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TransactionDTO
    {
        public string Id { get; set; }
        public string CardId { get; set; }
        public DateTime Created { get; set; }
        public int Amount { get; set; }
        public int RefundedAmount { get; set; }
        public int CapturedAmount { get; set; }
        public int VoidedAmount { get; set; }
        public int PendingAmount { get; set; }
        public int DisputedAmount { get; set; }
        public CardDTO Card { get; set; }
        public string Currency { get; set; }
        public Dictionary<string, string> Custom { get; set; }
        public bool Successful { get; set; }
        public string Descriptor { get; set; }
        public TrailDTO[] Trail { get; set; }

        public TransactionDTO(Transaction trans)
        {
            this.Id = trans.Id;
            this.CardId = trans.CardId;
            this.Created = trans.Created;
            this.Amount = trans.Amount;
            this.RefundedAmount = trans.RefundedAmount;
            this.CapturedAmount = trans.CapturedAmount;
            this.VoidedAmount = trans.VoidedAmount;
            this.PendingAmount = trans.PendingAmount;
            this.DisputedAmount = trans.DisputedAmount;
            this.Card = trans.Card != null ? new CardDTO(trans.Card) : null;
            this.Currency = trans.Currency;
            this.Custom = trans.Custom;
            this.Successful = trans.Successful;
            this.Descriptor = trans.Descriptor;
            this.Trail = trans.Trail != null ? trans.Trail.Select(x => new TrailDTO(x)).ToArray() : null;
        }
    }
}
