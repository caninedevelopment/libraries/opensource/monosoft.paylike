﻿namespace Monosoft.PayLike.Application.Resources.Transaction.Common
{
    using Monosoft.PayLike.Domain.Entities.Transaction;
    using System;

    public class CardDTO
    {
        public string Created { get; set; }
        public string Id { get; set; }
        public string Bin { get; set; }
        public string Last4 { get; set; }
        public DateTime Expiry { get; set; }
        public string Scheme { get; set; }
        public DateTime? Deleted { get; set; }

        public CardDTO(Card card)
        {
            this.Created = card.Created;
            this.Id = card.Id;
            this.Bin = card.Bin;
            this.Last4 = card.Last4;
            this.Expiry = card.Expiry;
            this.Scheme = card.Scheme;
            this.Deleted = card.Deleted;
        }
    }
}
