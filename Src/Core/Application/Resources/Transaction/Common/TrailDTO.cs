﻿namespace Monosoft.PayLike.Application.Resources.Transaction.Common
{
    using Monosoft.PayLike.Domain.Entities.Transaction;
    using System;

    public class TrailDTO
    {
        public FeeDTO Fee { get; set; }
        public int Amount { get; set; }
        public int Balance { get; set; }
        public DateTime Created { get; set; }
        public bool Capture { get; set; }
        public bool Refund { get; set; }
        public string Descriptor { get; set; }

        public TrailDTO(Trail trail)
        {
            this.Fee = new FeeDTO(trail.Fee);
            this.Amount = trail.Amount;
            this.Balance = trail.Balance;
            this.Created = trail.Created;
            this.Capture = trail.Capture;
            this.Refund = trail.Refund;
            this.Descriptor = trail.Descriptor;
        }
    }
}
