﻿namespace Monosoft.PayLike.Application.Resources.Merchant.Common
{
    using Monosoft.PayLike.Domain.Entities.Merchant;

    public class MerchantDTO : BaseMerchantDTO
    {
        public string Id { get; set; }
        public string Key { get; set; }

        public MerchantDTO() { }

        public MerchantDTO(Merchant m)
        {
            this.Id = m.Id;
            this.Key = m.Key;
            this.Name = m.Name;
            this.Test = m.Test;
            this.Website = m.Website;
            this.Email = m.Email;
            this.Descriptor = m.Descriptor;
            this.Currency = m.Currency;
            this.Company = new CompanyDTO(m.Company);
            this.Bank = new BankDTO(m.Bank);
        }
    }
}
