﻿namespace Monosoft.PayLike.Application.Resources.Merchant.Common
{
    using Monosoft.PayLike.Domain.Entities.Merchant;

    public class BankDTO
    {
        public string IBAN { get; set; }

        public BankDTO() { }

        public BankDTO(Bank b)
        {
            this.IBAN = b.IBAN;
        }
    }
}
