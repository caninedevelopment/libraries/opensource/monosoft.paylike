﻿namespace Monosoft.PayLike.Application.Resources.Merchant.Common
{
    using Monosoft.PayLike.Domain.Entities.Merchant;

    public class CompanyDTO
    {
        public string Country { get; set; }

        public string Number { get; set; }

        public CompanyDTO() { }

        public CompanyDTO(Company c)
        {
            this.Country = c.Country;
            this.Number = c.Number;
        }
    }
}
