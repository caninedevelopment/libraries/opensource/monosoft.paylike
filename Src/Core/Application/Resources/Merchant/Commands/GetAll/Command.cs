namespace Monosoft.PayLike.Application.Resources.Merchant.Commands.GetAll
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.PayLike.Domain.Interfaces;

    public class Command : IFunction<Response>
    {
        private readonly IPayLikeMerchant merc;

        public Command(
            IPayLikeMerchant merc)
        {
            this.merc = merc;
        }

        public Response Execute()
        {
            var mercs = merc.GetAll();
            return new Response(mercs);
        }
    }
}