namespace Monosoft.PayLike.Application.Resources.Merchant.Commands.GetAll
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.PayLike.Application.Resources.Merchant.Common;
    using Monosoft.PayLike.Domain.Entities.Merchant;
    using System.Collections.Generic;
    using System.Linq;

    public class Response : IDtoResponse
    {
        public List<MerchantDTO> Merchants { get; set; }

        public Response(List<Merchant> mercs)
        {
            this.Merchants = mercs.Select(x => new MerchantDTO(x)).ToList();
        }
    }
}