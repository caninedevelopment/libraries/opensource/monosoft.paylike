namespace Monosoft.PayLike.Application.Resources.Merchant.Commands.Insert
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Monosoft.PayLike.Application.Resources.Merchant.Common;

    public class Request : BaseMerchantDTO, IDtoRequest
    {
        public void Validate()
        {
            if (string.IsNullOrEmpty(Name))
            {
                throw new ValidationException(nameof(Name));
            }

            if (string.IsNullOrEmpty(Descriptor))
            {
                throw new ValidationException(nameof(Descriptor));
            }

            if (string.IsNullOrEmpty(Currency))
            {
                throw new ValidationException(nameof(Currency));
            }

            if (string.IsNullOrEmpty(Website))
            {
                throw new ValidationException(nameof(Website));
            }

            if (string.IsNullOrEmpty(Email))
            {
                throw new ValidationException(nameof(Email));
            }

            if (Company == null)
            {
                throw new ValidationException(nameof(Company));
            }

            if (Bank == null && Test == false)
            {
                throw new ValidationException(nameof(Bank));
            }
        }
    }
}