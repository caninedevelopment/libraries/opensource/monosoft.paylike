namespace Monosoft.PayLike.Application.Resources.Merchant.Commands.Insert
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.PayLike.Domain.Entities.Merchant;
    using Monosoft.PayLike.Domain.Interfaces;

    public class Command : IFunction<Request, Response>
    {
        private readonly IPayLikeMerchant merc;

        public Command(
            IPayLikeMerchant merc)
        {
            this.merc = merc;
        }

        public Response Execute(Request input)
        {
            var merchant = new Merchant(
                input.Name, 
                input.Currency, 
                input.Email, 
                input.Website, 
                input.Descriptor, 
                new Company(input.Company.Country, input.Company.Number), 
                new Bank(input.Bank?.IBAN));

            var mercRes = merc.Insert(merchant);

            return new Response(mercRes);
        }
    }
}