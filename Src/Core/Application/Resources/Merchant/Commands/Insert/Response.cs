﻿namespace Monosoft.PayLike.Application.Resources.Merchant.Commands.Insert
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.PayLike.Domain.Entities.Merchant;

    public class Response : IDtoResponse
    {
        public string Key { get; set; }
        public string Id { get; set; }

        public Response(Merchant merc)
        {
            this.Key = merc.Key;
            this.Id = merc.Id;
        }
    }
}