﻿namespace Monosoft.PayLike.Domain.Interfaces
{
    using Monosoft.PayLike.Domain.Entities.Merchant;
    using System.Collections.Generic;

    public interface IPayLikeMerchant
    {
        List<Merchant> GetAll(int limit = 30);
        Merchant Insert(Merchant merc);
    }
}
