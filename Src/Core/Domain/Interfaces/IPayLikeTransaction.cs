﻿namespace Monosoft.PayLike.Domain.Interfaces
{
    using Monosoft.PayLike.Domain.Entities.Transaction;
    using System.Collections.Generic;

    public interface IPayLikeTransaction
    {
        List<Transaction> GetAll(string merchantId, int limit = 30);
        void Insert(string merchantId, Transaction transaction);
    }
}
