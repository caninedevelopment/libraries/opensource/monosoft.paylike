﻿namespace Monosoft.PayLike.Domain.Entities.Transaction
{
    public class Fee
    {
        public int Flat { get; set; }
        public int Rate { get; set; }
    }
}
