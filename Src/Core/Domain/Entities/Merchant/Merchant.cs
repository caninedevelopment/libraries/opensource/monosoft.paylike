﻿namespace Monosoft.PayLike.Domain.Entities.Merchant
{
    public class Merchant
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string Currency { get; set; }

        public bool Test { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string Descriptor { get; set; }

        public Company Company { get; set; }

        public Bank Bank { get; set; }

        public Merchant(string name, string currency, string email, string website,  string descriptor, Company company, Bank bank)
        {
            this.Name = name;
            this.Currency = currency;
            this.Email = email;
            this.Website = website;
            this.Descriptor = descriptor;
            this.Company = company;
            this.Bank = bank;
        }
    }
}
