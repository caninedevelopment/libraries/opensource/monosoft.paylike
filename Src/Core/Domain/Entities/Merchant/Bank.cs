﻿namespace Monosoft.PayLike.Domain.Entities.Merchant
{
    public class Bank
    {
        public string IBAN { get; set; }

        public Bank(string iBan)
        {
            this.IBAN = iBan;
        }
    }
}
