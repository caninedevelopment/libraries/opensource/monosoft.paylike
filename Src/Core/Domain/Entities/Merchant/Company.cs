﻿namespace Monosoft.PayLike.Domain.Entities.Merchant
{
    public class Company
    {
        public string Country { get; set; }

        public string Number { get; set; }

        public Company(string country, string number)
        {
            this.Country = country;
            this.Number = number;
        }
    }
}
