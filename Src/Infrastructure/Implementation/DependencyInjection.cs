namespace Monosoft.PayLike.Implementation
{
    using Microsoft.Extensions.DependencyInjection;
    using Monosoft.PayLike.Domain.Interfaces;
    using Monosoft.PayLike.Implementation.Services;

    public static class DependencyInjection
    {
        public static IServiceCollection AddImplementation(this IServiceCollection services, string appId, string appKey)
        {
            GlobalVars.AppId = appId;
            GlobalVars.AppKey = appKey;

            services.AddSingleton(x => new PayLikeClient(appKey));
            services.AddSingleton<IPayLikeMerchant, PayLikeMerchant>();
            services.AddSingleton<IPayLikeTransaction, PayLikeTransaction>();

            return services;
        }
    }
}