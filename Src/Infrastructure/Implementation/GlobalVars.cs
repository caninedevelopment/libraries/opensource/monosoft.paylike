﻿namespace Monosoft.PayLike.Implementation
{
    public static class GlobalVars
    {
        public static string AppKey { get; set; }
        public static string AppId { get; set; }
    }
}
