﻿namespace Monosoft.PayLike.Implementation.Services
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Serialization;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    public class PayLikeClient
    {
        private readonly HttpClient _httpClient;
        private readonly JsonSerializer _jsonSerializer;
        private readonly JsonSerializerSettings _jsonSettings;

        public PayLikeClient(string key = null)
        {
            this._httpClient = new HttpClient();
            this._httpClient.BaseAddress = new Uri("https://api.paylike.io/");

            if (key != null)
            {
                this.SetApiKey(key);
            }

            _jsonSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };

            _jsonSerializer = new JsonSerializer()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        /// <summary>
        /// Set api key.
        /// </summary>
        /// <param name="key"></param>
        public void SetApiKey(string key)
        {
            _httpClient.DefaultRequestHeaders.Remove("Authorization");
            _httpClient.DefaultRequestHeaders.Add("Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($":{key}"))}");
        }

        public async Task<PaylikeApiResponse<T>> MakePaylikeApiRequest<T>(string urlPath, object requestObject = null, bool isDelete = false) where T : class
        {
            HttpResponseMessage msg = null;
            if (isDelete)
            {
                msg = await _httpClient.DeleteAsync(urlPath);
            }
            else
            {
                if (requestObject == null)
                {
                    msg = await _httpClient.GetAsync(urlPath);
                }
                else
                {
                    msg = await _httpClient.PostAsync(urlPath, new StringContent(JsonConvert.SerializeObject(requestObject, _jsonSettings), Encoding.UTF8, "application/json"));
                }
            }

            if (msg.IsSuccessStatusCode)
            {
                return new PaylikeApiResponse<T>(DeserializeAndUnwrap<T>(await msg.Content.ReadAsStringAsync()));
            }

            return new PaylikeApiResponse<T>(await HandleError(msg));
        }

        private async Task<ErrorResponse> HandleError(HttpResponseMessage request)
        {
            var errorString = await request.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(errorString))
            {
                return new ErrorResponse() { code = (int)request.StatusCode, message = request.ReasonPhrase };
            }
            var token = JToken.Parse(errorString);
            if (token is JArray)
            {
                return JsonConvert.DeserializeObject<List<ErrorResponse>>(errorString, _jsonSettings).FirstOrDefault();
            }
            else
            {
                return JsonConvert.DeserializeObject<ErrorResponse>(errorString, _jsonSettings);
            }
        }

        private T DeserializeAndUnwrap<T>(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return default(T);
            }
            if (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(List<>))
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            return JObject.Parse(json)[typeof(T).Name.ToLower()].ToObject<T>(_jsonSerializer);
        }
    }

    public class PaylikeApiResponse<T> where T : class
    {
        private ErrorResponse _errorResponse { get; set; }

        public bool IsSuccessStatusCode { get; set; }
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public T Result { get; set; }

        public PaylikeApiResponse()
        {
            IsSuccessStatusCode = false;
        }

        public PaylikeApiResponse(ErrorResponse error)
        {
            IsSuccessStatusCode = false;
            ErrorMessage = error?.message;
            ErrorCode = error?.code;
            _errorResponse = error;
        }

        public PaylikeApiResponse(T source)
        {
            Result = source;
            IsSuccessStatusCode = true;
        }

        public ErrorResponse GetError()
        {
            return _errorResponse;
        }


    }

    public class ErrorResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public bool client { get; set; }
        public bool merchant { get; set; }
        public TdsChallenge tds { get; set; }
    }

    public class TdsChallenge
    {
        public string url { get; set; }
        public string pareq { get; set; }
        public string oid { get; set; }
    }
}
