﻿namespace Monosoft.PayLike.Implementation.Services
{
    using Monosoft.PayLike.Domain.Entities.Transaction;
    using Monosoft.PayLike.Domain.Interfaces;
    using System;
    using System.Collections.Generic;

    public class PayLikeTransaction : IPayLikeTransaction
    {
        private readonly PayLikeClient client;

        public PayLikeTransaction(
            PayLikeClient client)
        {
            this.client = client;
        }

        public List<Transaction> GetAll(string merchantId, int limit = 30)
        {
            var res = client.MakePaylikeApiRequest<List<Transaction>>($"merchants/{merchantId}/transactions?limit={limit}").Result;

            if (res.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + res.ErrorMessage);
            }
            return res.Result;
        }

        public void Insert(string merchantId, Transaction transaction)
        {
            var source = new { cardId = transaction.CardId, descriptor = transaction.Descriptor, currency = transaction.Currency, amount = transaction.Amount };
            var res = client.MakePaylikeApiRequest<Transaction>($"merchants/{merchantId}/transactions", source).Result;

            if(res.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + res.ErrorMessage);
            }
        }
    }
}
