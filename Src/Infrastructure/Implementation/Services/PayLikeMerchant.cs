﻿namespace Monosoft.PayLike.Implementation.Services
{
    using Monosoft.PayLike.Domain.Entities.Merchant;
    using Monosoft.PayLike.Domain.Interfaces;
    using System;
    using System.Collections.Generic;

    public class PayLikeMerchant : IPayLikeMerchant
    {
        private readonly PayLikeClient client;

        public PayLikeMerchant(
            PayLikeClient client)
        {
            this.client = client;
        }

        public List<Merchant> GetAll(int limit = 30)
        {
            var res = client.MakePaylikeApiRequest<List<Merchant>>($"identities/{GlobalVars.AppId}/merchants?limit={limit}", null).Result;

            if (res.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + res.ErrorMessage);
            }
            return res.Result;
        }

        public Merchant Insert(Merchant merc)
        {
            var res = client.MakePaylikeApiRequest<Merchant>($"merchants", merc).Result;

            if(res.IsSuccessStatusCode == false)
            {
                throw new Exception("Error from paylike: " + res.ErrorMessage);
            }

            return res.Result;
        }
    }
}
